# love-connection-playnoob-dev-test

This project is to test developer skill. Please keep this project and it's content strictly confidential. Please download and/or clone to get the unity project.

Task - Make this level with this project with this gameplay - [Love Connection Demo Video](https://youtu.be/g1BAeZLfFQU) and send an apk to *playnoobstudio@gmail.com*

Ref Game - [Flow Free](https://play.google.com/store/apps/details?id=com.bigduckgames.flow)

Helpfull Assets 
****
- [DOTween](https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676) 
- [DOTween Documentation](http://dotween.demigiant.com/documentation.php)
- [Easings](https://easings.net/)
- [Particle Shape FX](https://www.dropbox.com/s/xm60sl0ivsvecu9/Shape%20FX.unitypackage?dl=0)
- [Shape FX Documentation](http://jeanmoreno.com/unity/shapefx/Documentation/ShapeFX%20Documentation.html)
